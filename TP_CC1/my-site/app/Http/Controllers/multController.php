<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class multController extends Controller
{
    private $users = [
        ['id' => 1, 'name' => 'user1', 'email' => 'user1@domaine.com', 'password' => 'user1pwd'],
        ['id' => 2, 'name' => 'user2', 'email' => 'user2@domaine.com', 'password' => 'user2pwd'],
        ['id' => 3, 'name' => 'user3', 'email' => 'user3@domaine.com', 'password' => 'user3pwd']
    ];

    public function calc($valeur1, $valeur2) {
        resultat = $valeur1 * $valeur2;
        return view('result', ['resultat' => resultat]);
    }